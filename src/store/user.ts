import {defineStore} from "pinia";

export const useUserStore = defineStore('user', {
  state: () => ({
    userId: 1 // L'ID de l'utilisateur par défaut
  }),
  actions: {
    setUserId(id: number) {
      this.userId = id;
    }
  },
  getters: {
    getUserId: (state) => state.userId
  }
});