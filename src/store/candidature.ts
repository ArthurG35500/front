import {Cooptation} from "../models/Cooptation.ts";
import {defineStore} from "pinia";
import {useUserStore} from "./user.ts";

export const useCandidatureStore = defineStore('candidature', {
    state: () => ({
        candidatures: [
            {
                id: 11,
                firstName: 'Alexandre',
                lastName: 'Deniel',
                specialty: 'DevSecOps',
                cvUrl: 'http://example.com/cv-alexandre.pdf',
                step: 2,
                by_user: 1
            },
            {
                id: 2,
                firstName: 'Marie',
                lastName: 'Curie',
                specialty: 'Data Scientist',
                cvUrl: 'http://example.com/cv-marie.pdf',
                step: 0,
                by_user: 2
            },
            {
                id: 10,
                firstName: 'Albert',
                lastName: 'Einstein',
                specialty: 'Analyste',
                cvUrl: 'http://example.com/cv-albert.pdf',
                step: 999,
                by_user: 2
            },
            {
                id: 4,
                firstName: 'Jordan',
                lastName: 'Newton',
                specialty: 'Data Scientist',
                cvUrl: 'http://example.com/cv-isaac.pdf',
                step: 5,
                by_user: 1
            },
            {
                id: 5,
                firstName: 'Fred',
                lastName: 'Newton',
                specialty: 'Infratructure',
                cvUrl: 'http://example.com/cv-isaac.pdf',
                step: 5,
                by_user: 1
            },
            {
                id: 6,
                firstName: 'Harry',
                lastName: 'Potter',
                specialty: 'Develloper',
                cvUrl: 'http://example.com/cv-isaac.pdf',
                step: 5,
                by_user: 1
            },
            {
                id: 7,
                firstName: 'Louis',
                lastName: 'Netro',
                specialty: 'DevOps',
                cvUrl: 'http://example.com/cv-isaac.pdf',
                step: 5,
                by_user: 1
            }
        ] as Cooptation[]
    }),
    actions: {
        incrementStep(id: number) {
            const candidature = this.candidatures.find(c => c.id === id);
            if (candidature && candidature.step < 999) {
                candidature.step += 1;
            }
        },
        rejectCandidature(id: number) {
            const candidature = this.candidatures.find(c => c.id === id);
            if (candidature) {
                candidature.step = 999;
            }
        },
        addCandidature(newCandidature: Cooptation) {
            this.candidatures.push(newCandidature);
        }
    },
    getters: {
        stepsZero: state => state.candidatures.filter(candidature => candidature.step === 0).sort((a, b) => b.id - a.id),
        otherSteps: state => state.candidatures.filter(candidature => candidature.step !== 0).sort((a, b) => b.id - a.id),
        candidaturesByUser: (state) => {
            const userStore = useUserStore();
            return state.candidatures
                .filter(candidature => candidature.by_user === userStore.userId)
                .sort((a, b) => b.id - a.id); // Tri par ID décroissant
        }
    }
});