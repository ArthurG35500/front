import {createRouter, createWebHistory} from "vue-router";


const routes = [
    {
        path: '/',
        component: () => import(/* webpackChunkName: "defaultlayout" */ '../layouts/defaultlayout/defaultlayout.vue'),
        children: [
            {
                path: '',
                name: 'Home',
                component: () => import(/* webpackChunkName: "home" */ '../views/home/home.vue'),
            },
            {
                path: 'cooptation',
                name: 'Cooptation',
                component: () => import(/* webpackChunkName: "copptation" */ '../views/cooptation/cooptation.vue'),
            },
            {
                path: 'candidature',
                name: 'Candidature',
                component: () => import(/* webpackChunkName: "candidature" */ '../views/candidature/candidature.vue'),
            }
        ]
    },
    // {
    //     path: '/login',
    //     component: Emptylayout,
    //     children: [
    //         {
    //             path: '',
    //             name: 'Login',
    //             component: Login
    //         }
    //     ]
    // }
]

const router = createRouter({
    history: createWebHistory(import.meta.env.VITE_BASE_URL),
    routes
});

export default router