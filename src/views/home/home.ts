import { defineComponent } from 'vue';
import Headercomponent from "../../components/headercomponent/headercomponent.vue";
import InfoPersocomponent from "../../components/infoPersocomponent/infoPersocomponent.vue";
import Goalsteepercomponent from "../../components/goalsteepercomponent/goalsteepercomponent.vue";
import BoxBagdecomponent from "../../components/boxBagdecomponent/boxBagdecomponent.vue";

export default defineComponent({
  name: 'home',
    components: {BoxBagdecomponent, Goalsteepercomponent, InfoPersocomponent, Headercomponent}
});
