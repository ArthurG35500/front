import {computed, defineComponent} from 'vue';
import Cooptationcomponent from "../../components/cooptationcomponent/cooptationcomponent.vue";
import {useCandidatureStore} from "../../store/candidature.ts";

export default defineComponent({
  name: 'cooptation',
  components: {Cooptationcomponent},
  setup() {
    const store = useCandidatureStore();
    const candidatures = computed(() => store.candidaturesByUser);
    const button = true;

    const incrementStep = (id: number) => {
      store.incrementStep(id);
    };

    const rejectCandidature = (id: number) => {
      store.rejectCandidature(id);
    };

    return {
      candidatures,
      button,
      incrementStep,
      rejectCandidature
    };
  },
  data(){
    return{
      buttonDisplay: false
    }
  }
});
