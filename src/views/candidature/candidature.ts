import {computed, defineComponent} from 'vue';
import Candidaturecomponent from "../../components/candidaturecomponent/candidaturecomponent.vue";
import CandidaturePowercomponent from "../../components/candidaturePowercomponent/candidaturePowercomponent.vue";
import {useCandidatureStore} from "../../store/candidature.ts";

export default defineComponent({
  name: 'candidature',
  components: {CandidaturePowercomponent, Candidaturecomponent},
  setup() {
    const store = useCandidatureStore();
    const candidatures = computed(() => store.candidatures);
    const button = true;

    const incrementStep = (id: number) => {
      store.incrementStep(id);
    };

    const rejectCandidature = (id: number) => {
      store.rejectCandidature(id);
    };

    return {
      candidatures,
      button,
      incrementStep,
      rejectCandidature
    };
  },
  data() {
    return {
      buttonDisplay: true
    };
  }
});
