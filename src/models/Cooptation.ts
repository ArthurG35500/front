export interface Cooptation {
    id: number;
    firstName: string;
    lastName: string;
    specialty: string;
    cvUrl: string;
    step: number;
    by_user: number;
}