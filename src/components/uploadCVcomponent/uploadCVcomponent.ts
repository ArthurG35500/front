import {defineComponent, ref} from 'vue';
import {useUserStore} from "../../store/user.ts";
import {useCandidatureStore} from "../../store/candidature.ts";

export default defineComponent({
  name: 'uploadCVcomponent',
  setup() {
    const form = ref({
      nom: '',
      prenom: '',
      specialite: '',
      fileName: ''
    });

    const fileInput = ref<HTMLInputElement | null>(null);
    const candidatureStore = useCandidatureStore();
    const userStore = useUserStore();

    const handleFileUpload = (event: Event) => {
      const target = event.target as HTMLInputElement;
      if (target.files && target.files.length > 0) {
        const file = target.files[0];
        form.value.fileName = file.name
        console.log('File uploaded:', file);
      }
    };

    const handleDrop = (event: DragEvent) => {
      if (event.dataTransfer && event.dataTransfer.files.length > 0) {
        if (fileInput.value) {
          fileInput.value.files = event.dataTransfer.files;
          form.value.fileName = fileInput.value.files[0].name
        }
      }
    };

    const submitForm = () => {
      console.log('Form submitted:', form.value);
      const newCandidature = {
        id: Date.now(), // Generate a unique ID
        firstName: form.value.prenom,
        lastName: form.value.nom,
        specialty: form.value.specialite,
        cvUrl: form.value.fileName,
        step: 0,
        by_user: userStore.userId
      };
      candidatureStore.addCandidature(newCandidature);
      // Reset the form
      form.value.nom = '';
      form.value.prenom = '';
      form.value.specialite = '';
      form.value.fileName = '';
      if (fileInput.value) {
        fileInput.value.value = '';
      }
    };

    return {
      form,
      fileInput,
      handleFileUpload,
      handleDrop,
      submitForm,
    };
  },
});
