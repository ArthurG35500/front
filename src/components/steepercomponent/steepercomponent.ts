import {computed, defineComponent, PropType} from 'vue';

export default defineComponent({
  name: 'steepercomponent',
  props: {
    chiffre: {
      type: Number as PropType<number>,
      required: true
    },
    point: {
      type: Number as PropType<number>,
      required: true
    },
    goal: {
      type: Number as PropType<number>,
      required: true
    }
  },
  setup(props) {
    const isGoalMet = computed(() => props.chiffre >= props.goal);
    return {
      isGoalMet,
    };
  },
});
