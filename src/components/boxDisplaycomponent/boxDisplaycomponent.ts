import {defineComponent, PropType} from 'vue';

export default defineComponent({
  name: 'boxDisplaycomponent',
  props: {
    score: {
      type: Number as PropType<number>,
      required: true
    },
    title: {
      type: String as PropType<string>,
      required: true
    }
  }
});
