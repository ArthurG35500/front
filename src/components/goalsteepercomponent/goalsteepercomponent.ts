import {defineComponent, ref} from 'vue';
import Steepercomponent from "../steepercomponent/steepercomponent.vue";

export default defineComponent({
  name: 'goalsteepercomponent',
    components: {Steepercomponent},
  setup(){
    const chiffre = ref(4);
    return{ chiffre}

  }
});
