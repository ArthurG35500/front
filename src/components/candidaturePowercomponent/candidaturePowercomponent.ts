import {defineComponent, PropType} from 'vue';
import TrackerCooptationcomponent from "../trackerCooptationcomponent/trackerCooptationcomponent.vue";
import {Cooptation} from "../../models/Cooptation.ts";

export default defineComponent({
  name: 'candidaturePowercomponent',
    components: {TrackerCooptationcomponent},
  props: {
    candidatures: {
      type: Array as PropType<Cooptation[]>,
      required: true
    },
    buttonDisplay:{
      type: Boolean as PropType<boolean>,
      required: true
    }
  },
  data() {
    return {
      isAdmin: true,
    };
  },
  methods: {
    incrementStep(id: number) {
      this.$emit('increment-step', id);
    },
    rejectCandidature(id: number) {
      this.$emit('reject-candidature', id);
    }
  }
});
