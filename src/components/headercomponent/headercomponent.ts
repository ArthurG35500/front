import { defineComponent } from 'vue';
import Namecomponent from "../namecomponent/namecomponent.vue";
import Menucomponent from "../menucomponent/menucomponent.vue";

export default defineComponent({
  name: 'headercomponent',
    components: {Menucomponent, Namecomponent}
});
