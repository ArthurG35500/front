import { defineComponent } from 'vue';
import {useRoute} from "vue-router";

export default defineComponent({
  name: 'menucomponent',
  setup() {
    const route = useRoute();

    const isActive = (path: string): boolean => {
      return route.path === path || route.hash === path;
    };

    return {
      isActive,
    };
  },
});

