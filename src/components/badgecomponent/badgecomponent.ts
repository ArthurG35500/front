import {defineComponent, PropType} from 'vue';

export default defineComponent({
  name: 'badgecomponent',
  props: {
    logo: {
      type: String as PropType<string>,
      required: true
    },
    titre:{
      type: String as PropType<string>,
      required: true
    }
  },
});
