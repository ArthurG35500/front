import {defineComponent, PropType} from 'vue';
import UploadCVcomponent from "../uploadCVcomponent/uploadCVcomponent.vue";
import CooptationListcomponent from "../cooptationListcomponent/cooptationListcomponent.vue";
import {Cooptation} from "../../models/Cooptation.ts";

export default defineComponent({
  name: 'cooptationcomponent',
  components: {CooptationListcomponent, UploadCVcomponent},
  props: {
    candidatures: {
      type: Array as PropType<Cooptation[]>,
      required: true
    },
    buttonDisplay: {
      type: Boolean as PropType<boolean>,
      required: true
    }
  },
  methods: {
    incrementStep(id: number) {
      const candidature = this.candidatures.find(c => c.id === id);
      if (candidature && candidature.step < 999) {
        candidature.step += 1;
      }
    },
    rejectCandidature(id: number) {
      const candidature = this.candidatures.find(c => c.id === id);
      if (candidature) {
        candidature.step = 999;
      }
    }
  }
});
