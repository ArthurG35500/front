import {defineComponent, PropType} from 'vue';
import UploadCVcomponent from "../uploadCVcomponent/uploadCVcomponent.vue";
import CooptationListcomponent from "../cooptationListcomponent/cooptationListcomponent.vue";
import CandidaturePowercomponent from "../candidaturePowercomponent/candidaturePowercomponent.vue";
import {Cooptation} from "../../models/Cooptation.ts";

export default defineComponent({
  name: 'candidaturecomponent',
    components: {CandidaturePowercomponent, CooptationListcomponent, UploadCVcomponent},
  props: {
    candidatures: {
      type: Array as PropType<Cooptation[]>,
      required: true
    },
    buttonDisplay:{
      type: Boolean as PropType<boolean>,
      required: true
    }
  },
  computed: {
    stepsZero(): Cooptation[] {
      return this.candidatures.filter(candidature => candidature.step === 0);
    },
    otherSteps(): Cooptation[] {
      return this.candidatures.filter(candidature => candidature.step !== 0);
    }
  },
  methods: {
    incrementStep(id: number) {
      const candidature = this.candidatures.find(c => c.id === id);
      if (candidature && candidature.step < 999) {
        candidature.step += 1;
      }
    },
    rejectCandidature(id: number) {
      const candidature = this.candidatures.find(c => c.id === id);
      if (candidature) {
        candidature.step = 999;
      }
    }
  }
});
