import {defineComponent, PropType} from 'vue';
import {Cooptation} from "../../models/Cooptation.ts";

export default defineComponent({
  name: 'trackerCooptationcomponent',
  props: {
    admin: {
      type: Boolean as PropType<boolean>,
      required: true
    },
    step: {
      type: Number as PropType<number>,
      required: true
    },
    cooptation: {
      type: Object as PropType<Cooptation>,
      required: true
    },
    buttonDisplay: {
      type: Boolean as PropType<boolean>,
      required: true
    }
  },
  data() {
    return {
      steps: [
        'Entretien téléphonique',
        'Entretien RH',
        'Entretien Manager',
        'Candidat recruté'
      ]
    };
  },
  methods: {
    getStepClass(index: number) {
      if (this.step === 999) return 'text-red-500';
      if (this.step === 0) return '';
      if (index < this.step - 1) return 'text-greenLight';
      if (index === this.step - 1) return 'text-orange-500';
      return '';
    },
    incrementStep() {
      this.$emit('increment-step', this.cooptation.id);
    },
    rejectCandidature() {
      this.$emit('reject-candidature', this.cooptation.id);
    }
  }
});
