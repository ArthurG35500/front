const fs = require('fs');
const path = require('path');

function createDirectory(dir) {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }
}

function createFile(filePath, content) {
    fs.writeFileSync(filePath, content);
}

function createStore(name) {
    const fullname = `${name}store`;
    const dir = `./src/stores/${fullname}`;
    createDirectory(dir);

    const tsContent = `import { defineStore } from 'pinia';\n\nexport const use${capitalize(name)}Store = defineStore('${name}', {\n  state: () => ({\n    items: []\n  }),\n  getters: {\n    getItems: (state) => state.items\n  },\n  actions: {\n    async fetchItems() {\n      const items = await fetch('/api/items').then(res => res.json());\n      this.items = items;\n    }\n  }\n});\n`;

    createFile(path.join(dir, `${fullname}.ts`), tsContent);
}

function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

const componentName = process.argv[2];
if (!componentName) {
    console.log('Please provide a store name');
    process.exit(1);
}

createStore(componentName);
console.log(`Store module ${componentName} created successfully`);
