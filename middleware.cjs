const fs = require('fs');
const path = require('path');

function createMiddleware(name) {
    const filename = `${name}.ts`;
    const dir = './src/middlewares';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }

    // Contenu du middleware
    const middlewareContent = `import type {Middleware} from "@nuxt/types";

const ${name}: Middleware = ({ store, redirect }) => {

};

export default ${name};
`;

    fs.writeFileSync(path.join(dir, filename), middlewareContent);
}

const middlewareName = process.argv[2];
if (!middlewareName) {
    console.log('Please provide a middleware name');
    process.exit(1);
}

createMiddleware(middlewareName);
console.log(`Middleware ${middlewareName} created successfully`);
