module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  "./node_modules/flowbite/**/*.js",
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
  ],
  theme: {
    extend: {
      height: {
        'max-calc': 'calc(100vh - 60px - 60px)',
      },
      colors: {
        'greenLight': '#D0FFE9',
        'greenLight2': '#D2ECDE',
        'greenDark': '#51767A',
        'greenDark2': '#2F4547',
        'backgroundGreen': '#0D1414'
      },
      backgroundImage: {
        'greenLight': '#D0FFE9',
        'greenLight2': '#D2ECDE',
        'greenDark': '#51767A',
        'greenDark2': '#2F4547',
        'backgroundGreen': '#0D1414',
        '6tm': 'linear-gradient(3.69deg, #000 15.69%, rgba(0,0,0,0) 46.33%),linear-gradient(0deg, #1F1F1F, #1F1F1F)',
        'component': 'background: rgb(22 22 22 / 43%);'
      },
      borderColor: {
        'color': 'rgba(81, 118, 122, 0.3)',
        'greenLight': '#D0FFE9',
      },
      ringColor:{
        'greenLight': '#D0FFE9',
      }
    },
  },
  plugins: [
    require('flowbite/plugin')
  ]
}