const fs = require('fs');
const path = require('path');

function createComponent(name) {
    const fullname = `${name}component`
    const dir = `./src/components/${fullname}`;
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir, { recursive: true });
    }

    // Création des fichiers après la création du dossier
    const vueContent = `<template>\n  <div>\n\n  </div>\n</template>\n\n<script lang="ts" src="./${fullname}.ts"></script>\n\n<style scoped lang="scss" src="./${fullname}.scss"></style>\n`;
    const tsContent = `import { defineComponent } from 'vue';\n\nexport default defineComponent({\n  name: '${fullname}'\n});\n`;
    const scssContent = `// Styles for ${fullname}\n`;

    fs.writeFileSync(path.join(dir, `${fullname}.vue`), vueContent);
    fs.writeFileSync(path.join(dir, `${fullname}.ts`), tsContent);
    fs.writeFileSync(path.join(dir, `${fullname}.scss`), scssContent);
}

const componentName = process.argv[2];
if (!componentName) {
    console.log('Please provide a page name');
    process.exit(1);
}

createComponent(componentName);
console.log(`Component ${componentName} created successfully`);
