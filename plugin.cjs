const fs = require('fs');
const path = require('path');

function createPlugin(name) {
    const filename = `${name}.ts`;
    const dir = './src/plugins';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }

    // Contenu du plugin
    const pluginContent = `const ${name}: Plugin = ({ $config }, inject) => {

}

export default ${name}
`;

    fs.writeFileSync(path.join(dir, filename), pluginContent);
}

const pluginName = process.argv[2];
if (!pluginName) {
    console.log('Please provide a plugin name');
    process.exit(1);
}

createPlugin(pluginName);
console.log(`Plugin ${pluginName} created successfully`);