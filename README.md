# Mon Application Vue 3 TypeScript

## Description

Application de coopatage 6TM développer dans le cadre du hackathon 2024 de Sup De Vinci.

# UML

```plantuml
@startuml
class User {
  -id : int
  -email : string
  -admin : boolean
  -score : int
}

class Recommendation {
  -id : int
  -recommendation_date : Date
  -status : string
  -user_id : int
  -linkedin_url : string
  -cv_url : string
  -job : string
  -speciality: string
}

class Badge {
  -id : int
  -name : string
  -description : string
}

class UserBadge {
  -user_id : int
  -badge_id : int
  -date_awarded : Date
}

class Team {
  -id : int
  -name : string
  -score : int
}

User "1" -- "0..*" Recommendation : makes
User "1" -- "0..*" UserBadge : earns
Badge "1" -- "0..*" UserBadge : awarded to
User "0..*" -- "1" Team : belongs to
@enduml
```

# Déployement de l'Application
